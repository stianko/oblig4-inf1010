// Subklasse for legemidler uten spesiell klassifikasjon (C) av typen piller
class LegemidlerCPiller extends LegemidlerC implements Piller{
    LegemidlerCPiller(String navn, int nummer, int pris, String type, int mengde){
        super(navn, nummer, pris, type, mengde);
     } 
}
