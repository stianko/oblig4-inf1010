import java.util.*;

//Tar bare i mot objekter som implementerer comparable og lik.
//Klassen implementerer videre nodvendige grensesnitt
public class SortertEnkelListe<T extends Comparable & Lik> implements AbstraktSortertEnkelListe<T>, Iterable<T>{

    private Node first, last;
    private int antall;

    private class Node{
        Node(T t) {
            objekt = t;
        }
        Node neste;
        T objekt;
    }

    SortertEnkelListe(){
        first = null;
        last = null;
    }
    // Setter inn objekt i listen
    public void set(T t){
        Node n = new Node(t);
        antall++;

        // Om lista er tom:
        if (last == null){
            first = n;
            last = n;
            n.neste = null;
        } else {
            Node denne = first;
            Node forrige = first;

            while(denne != null){
                if (n.objekt.compareTo(denne.objekt) < 0){
                    if (denne == first){
                        first = n;
                    } else {
                        forrige.neste = n;
                    }
                    n.neste = denne;
                    return;
                }
                forrige = denne;
                denne = denne.neste;
            }
            forrige.neste = n;
            last = n;
        }
	
    }
    // Henter et objekt med en String som parameter
    public T find(String s){
        for(T t : this){
                if(t.samme(s)){
                    return t;
                }
            }
	      return null;
    }
    //Returnerer iterator over listen
    public Iterator<T> iterator(){
      ListeIterator li = new ListeIterator();
      return li;
    }
    //Iteratorklassen med nodvendige metoder for iteratoren
    private class ListeIterator implements Iterator<T>{
        Node neste = null;

        public ListeIterator(){
            neste = first;
        }

        public boolean hasNext(){
            return neste != null;
        }

        public T next(){
            if(hasNext()){
                T ut = neste.objekt;
                neste = neste.neste;
                return ut;
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove(){
            throw new UnsupportedOperationException();
        }
    }
}
