// Subklasse for vanedannende midler (B) av typen liniment
class LegemidlerBLiniment extends LegemidlerB implements Liniment{
    LegemidlerBLiniment(String navn, int nummer, int pris, String type, int mengde, int vanedannende){
        super(vanedannende, navn, nummer, pris, type, mengde);
     } 
}
