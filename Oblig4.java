//INF1010
//OBLIGATORISK OPPGAVE 4
//VAAR 2014
//stiako og mathiapk

import java.util.*;
import java.io.*;
//Main-metode og nodvendige metodekall
class Oblig4{
    public static void main(String[] args){
        Mainclass m = new Mainclass();
        m.readFile();
        m.programLoop();
    }
}
//Klassen med alle kjoringsmetoder.
//Oppretter nodvendige objekter av klassene og globale variabler
class Mainclass{
    Personer p = null;
    Tabell<Personer> t = new Tabell<>(20);
    Tabell<Legemidler> lmt = new Tabell<>(20);
    Legemidler lm = null;
    Leger leger = null;
    SortertEnkelListe<Leger> sel = new SortertEnkelListe<>();
    EnkelReseptListe erl = new EnkelReseptListe();
    Resepter res = null;
    Scanner input = new Scanner(System.in);
    int legemiddelTeller;
    int reseptTeller;
    int personTeller;
    //Leser inn filen "data.txt"
    public void readFile(){
        String tmp;
        Scanner read = null;
        try{
            read = new Scanner(new File("data.txt"));
	    //Loop som kjorer saa lenge det er tekst igjen i fila
	    //Sjekker hva som blir lest og sender info til ulike metoder
            while(read.hasNext()){
                tmp = read.nextLine();
                if(tmp.contains("# Personer")){
                    while(!tmp.isEmpty()){
                        tmp = read.nextLine();
                        parsePerson(tmp);
                    }
                } else if(tmp.contains("# Legemidler")){
                    while(!tmp.isEmpty()){
                        tmp = read.nextLine();
                        parseLegemidler(tmp);
                    }
                } else if(tmp.contains("# Leger")){
                    while(!tmp.isEmpty()){
                        tmp = read.nextLine();
                        parseLeger(tmp);
                    }
                } else if(tmp.contains("# Resepter")){
                    while(!tmp.isEmpty()){
                        tmp = read.nextLine();
                        parseResepter(tmp);
                    }
                }
            }

        }catch(IOException e){
            System.out.println("Kunne ikke lese fil");
        }
    }
    //Inneholder switch-case med alle terminalvalg.
    public void programLoop(){
        int valg = 0;
        Scanner enter = new Scanner(System.in);

        while(valg != 10){
            valg = meny();

            switch(valg){
                case 1: nyttLegemiddel();break;
                case 2: nyLege();break;
                case 3: nyPerson();break;
                case 4: nyResept();break;
                case 5: hentResept();break;
                case 6: printLLP();break;
                case 7: gyldigeBlaResepter();break;
                case 8: printAvtaleLeger();break;
                case 9: printVanedannende();break;
                case 10: break;
                default: System.out.println(valg + " er ikke et gyldig valg.\n");
            }
            if(valg != 10){
                System.out.println("\nTrykk ENTER for meny\n");
                enter.nextLine();
            }
        }
    }
    //Skriver ut menyen til skjerm og tar imot valget (input) fra terminal
    int meny(){
        int menyValg;

        for(int i = 0; i < 22; i++){
            System.out.print("-");
        }
        System.out.print("meny");
        for(int i = 0; i < 22; i++){
            System.out.print("-");
        }
        System.out.println();

        System.out.println("1. Registrer nytt legemiddel");
        System.out.println("2. Legg inn ny lege");
        System.out.println("3. Legg inn ny person");
        System.out.println("4. Opprett ny resept");
        System.out.println("5. Hent ut resept");
        System.out.println("6. Skriv ut legemidler, leger og personer");
        System.out.println("7. Skriv ut gyldige blaa resept for person");
        System.out.println("8. Skriv ut alle avtalelegers narkotiske resepter");
        System.out.println("9. Skriv ut persons gyldige resepter for vanedannende legemidler");
        System.out.println("10. Avslutt");

        System.out.print("\nVelg et alternativ (1-10): ");
        menyValg = input.nextInt();

        System.out.println("");

        return menyValg;
    }
    //Metode for aa legge inn nytt legemiddel
    //Bruker "parseLegemidler"-metoden under
    //Sender all info som en samlet string
    public void nyttLegemiddel(){
        legemiddelTeller++;
        String s;
        String form;
        String type;
        String navn;
        int pris;
        int mengde;
        int styrke;
        System.out.print("Skriv navn paa legemiddel: ");
        navn = input.next();
        System.out.print("Skriv type (A, B, C): ");
        type = input.next().toLowerCase();
        System.out.print("Skriv form (pille, injeksjon, liniment): ");
        form = input.next().toLowerCase();
        System.out.print("Skriv inn pris: ");
        pris = input.nextInt();
        System.out.print("Skriv inn mengde (antall, mg, cm3): ");
        mengde = input.nextInt();
        System.out.println();
        if(type.equals("a") || type.equals("b")){
            System.out.print("Skriv inn styrke (heltall): ");
            styrke = input.nextInt();
            s = "" + legemiddelTeller + ", " + navn + ", " + form + ", " + type + ", " + pris + ", " + mengde + ", " + styrke;
            parseLegemidler(s);
        }else{
            s = "" + legemiddelTeller + ", " + navn + ", " + form + ", " + type + ", " + pris + ", " + mengde;
            parseLegemidler(s);
        }
        
    }   
    //Metode for aa legge til ny lege
    public void nyLege(){
        boolean spesialist = false;
        System.out.print("Skriv navn: ");
        input.nextLine();
        String navn = input.nextLine();
        System.out.print("Spesialist? (j/n): ");
        if(input.next().equals("j")){
            spesialist = true;  
         }
        System.out.print("Skriv avtalenummer(heltall): ");
        int avtalenr = input.nextInt();
	//Oppretter nytt lege-objekt
        sel.set(new Leger(navn, spesialist, avtalenr));

    }
    //Metode for aa opprette ny person
    public void nyPerson(){
        boolean test = false;
        String kjonn = "";
        System.out.print("Skriv navn: ");
        input.nextLine();
        String navn = input.nextLine();
        while(!test){
            System.out.print("Skriv kjonn (m/k): ");
            kjonn = input.next();
            if(kjonn.equals("m") || kjonn.equals("k")){
                test = true;
            }
        }
	//Oppdaterer personnummer og oppretter nytt personobjekt
        personTeller++;
        t.setObject(personTeller, new Personer(navn, personTeller, kjonn));

      
    }    
    //Metode for aa opprette nytt resept
    //Bruker for-each lokker til aa skrive ut nodvendig informasjon
    public void nyResept(){
        System.out.print("Skriv type (h/b): ");
        String type = input.next();
        System.out.println();
        System.out.println("Skriv et personnummer fra listen under: ");
        for(Personer person: t){
            System.out.println(person.getNummer() + " " + person.getNavn());
        }
        int personnr = input.nextInt();
        System.out.println();
        System.out.println("Skriv et legenavn fra listen under: ");
        for(Leger lege: sel){
            System.out.println(lege.getNavn());
        }
        input.nextLine();
        Leger lege = sel.find(input.nextLine());
        System.out.println();
        System.out.println("Skriv et legemiddelnummer fra listen under: ");
        for(Legemidler l: lmt){
            System.out.println(l.getNr() + " " + l.getNavn());
        }
        Legemidler legemiddel = lmt.findObject(input.nextInt());
        System.out.println();
        System.out.print("Skriv inn antall uthentinger: ");
        int reit = input.nextInt();
	//Oppdaterer reseptnummer, oppretter ny resept, legger til resept i liste.
	//Legger til slutt resepten til i personens reseptliste
        reseptTeller++;
        Resepter tmpres = new Resepter(reseptTeller, type, personnr, lege, legemiddel, reit);
	erl.set(tmpres);
        t.findObject(personnr).getResept().set(tmpres);
    }  
    //Metode for aa hente ut resept
    //Skriver ut personer til skjermen, bruker velger person
    public void hentResept(){
        System.out.println("Velg personnummer fra listen under:"); 
        for(Personer person: t){
            System.out.println(person.getNummer() + " " + person.getNavn());
        }
        int personnr = input.nextInt();
        YngsteForstReseptListe y = t.findObject(personnr).getResept();
	//Skriver ut resepter til skjermen, bruker velger resept
        System.out.println("Velg reseptnummer fra listen under:");
        for(Resepter resept: y){
            System.out.print(resept.getNr() + " " + resept.getLegemiddel().getNavn());
            System.out.println(" antall uthentinger gjenstaar: " + resept.getReit());
        }
        int i = input.nextInt();
	//Sjekker om resepten er gyldig
        if(y.find(i).getReit() == 0){
            System.out.println();
            System.out.println("Resepten er ugyldig!");
            System.out.println();
        }else{
	    //Skriver ut info om resept + legemiddel
            Legemidler legemiddel = y.find(i).hentResept();
            System.out.println();
            System.out.println("Pasienten skal betale: " + legemiddel.getPris());
            System.out.println("Gjenstaande uthentinger paa resept: " + y.find(i).getReit());
            System.out.println("----------------------------------------");
            System.out.println("Utstedt av: " + y.find(i).getLege().getNavn());
            System.out.println("Pasientens navn: " + t.findObject(y.find(i).getPersonnr()).getNavn());
            System.out.println("Info om legemiddelet:");
            System.out.println("Navn: " + legemiddel.getNavn());
            System.out.println("Type: " + legemiddel.getType());
            System.out.print("Mengde: " + legemiddel.getMengde());
            System.out.println(" " + finnForm(legemiddel));

            System.out.println();
        }
    }
    //Metode for aa finne ut hva slags form legemiddelet har.
    //Tar i mot et objekt av Legemidler, returnerer en string
    public String finnForm(Legemidler legemiddel){
        String form = "";
        if(legemiddel instanceof LegemidlerAPiller){form = "piller";}
        if(legemiddel instanceof LegemidlerAInjeksjon){form = "mg";}
        if(legemiddel instanceof LegemidlerALiniment){form = "cm3";}
        if(legemiddel instanceof LegemidlerBPiller){form = "piller";}
        if(legemiddel instanceof LegemidlerBInjeksjon){form = "mg";}
        if(legemiddel instanceof LegemidlerBLiniment){form = "cm3";}
        if(legemiddel instanceof LegemidlerCPiller){form = "piller";}
        if(legemiddel instanceof LegemidlerCInjeksjon){form = "mg";}
        if(legemiddel instanceof LegemidlerCLiniment){form = "cm3";}

        return form;

    }
    //Metode for aa finne gyldige blaa resepter, 
    //hvor medisinen er av form injeksjon, hos en person.
    //Skriver ut personer til skjerm, bruker velger person
    public void gyldigeBlaResepter(){
        System.out.println("Velg personnummer fra listen under:");
        for(Personer p: t){
            System.out.println(p.getNummer() + " " + p.getNavn());
        }
        int personnr = input.nextInt();
        YngsteForstReseptListe y = t.findObject(personnr).getResept(); 
        //Her har vi gatt utifra at injeksjonsdoser, som skrevet i oppgaven,
        //tilsvarer "reit" for legemidler som inntas som injeksjon.
        System.out.println();
        System.out.println("Gyldige blaa resepter for " + t.findObject(personnr).getNavn() + ": "); 
        int res = 0;
	//itererer gjennom personens resepter, skriver ut de aktuelle.
        for(Resepter r: y){
            if(r.getType().equals("b") && r.getReit() > 0 && finnForm(r.getLegemiddel()).equals("mg")){
                System.out.println();
                System.out.println("Reseptnummer: " + r.getNr());
                System.out.println(r.getLegemiddel().getNavn());
                System.out.println("Gjenstaaende injeksjonsdoser: " + r.getReit());
                res++;
            } else {
                System.out.println(res); 
                System.out.println();
            }
        }
    }    
    //Metode for aa skrive ut relevant info om Legemidler, Leger og Personer
    public void printLLP(){
        System.out.println("Legemidler: ");
        for(Legemidler l : lmt){
            System.out.print(l.getNavn() + " " + l.getMengde());
            System.out.println(" " + finnForm(l) + " Pris: " + l.getPris());
        }
        System.out.println("-----------------------------------------\n");
        System.out.println("Leger: ");
        for(Leger l : sel){
            System.out.print(l.getNavn());
            if(l.getAvtaleLege()){
                System.out.print(", avtalenr: " + l.getAvtaleNr());
            }
            if(l.getSpesialist()){
                System.out.print(", Spesialist");
            }
            System.out.println();
        }
        System.out.println("-----------------------------------------\n");
        for(Personer p : t){
            System.out.print(p.getNavn() + ", kjonn: " + p.getKjonn());
            System.out.println(", Personnummer: " + p.getNummer());
        }
        System.out.println();
    }    
    //Skriver ut antall resepter, tilhorende avtaleleger, paa narkotiske stoffer.
    public void printAvtaleLeger(){
        EldsteForstReseptListe efrl;
        int teller = 0;
	//Itererer gjennom leger, deretter sjekker om legen har avtale
	//Dersom ja, itererer gjennom legens resepter og finner de aktuelle
        for(Leger l: sel){
            if(l.getAvtaleLege()){
                System.out.println("Navn: " + l.getNavn());
                efrl = l.getResept();
                for(Resepter r: efrl){
                    if(r.getLegemiddel().getType().equals("a")){
                        teller++;
                    }
                }
                System.out.println("Antall resepter for narkotiske legemidler: " + teller);
            }
        }
      
    } 
    //Skriver ut antall vanedannende medisiner utskrevet til personer.
    //Teller baade for antall til menn og kvinner + totalt.
    public void printVanedannende(){
        YngsteForstReseptListe y;
        int manneteller = 0;
        int dameteller = 0;
        for(Personer p: t){
            int pteller = 0;
            System.out.print(p.getNavn() + ", vanedannende resepter: ");
            y = p.getResept();
            for(Resepter r: y){
                if(r.getReit() > 0 && r.getLegemiddel().getType().equals("b")){
                    pteller++;
                    if(p.getKjonn().equals("m")){
                        manneteller++;
                    }else{
                        dameteller++;
                    }
                }
            }
            System.out.println(pteller);
        }
        int totalteller = manneteller + dameteller;
        System.out.println();
        System.out.println("Vanedannende medisiner utskrevet til menn: " + manneteller);
        System.out.println("Vanedannende medisiner utskrevet til kvinner: " + dameteller);
        System.out.println("Vanedannende medisiner utskrevet totalt: " + totalteller);
    }
    //Oppretter personer etter innlesing fra fil.
    //Mottar en string, splitter og lagrer data i variabler.
    private void parsePerson(String s){
        int persnr = 0;
        String navn = "";
        String kjonn = "";

        if(s.length() > 0){
            String[] parts = s.split(", ");
            persnr = Integer.parseInt(parts[0]);
            navn = parts[1];
            kjonn = parts[2];
            personTeller = persnr;
        }
	//Oppretter objektet og legger til i tabell
        p = new Personer(navn, persnr, kjonn);
        t.setObject(persnr, p);
        
    }
    //Oppretter nye legemidler etter innlesing av fil.
    //Tar imot en string, splitter og lagrer data i variabler.
    private void parseLegemidler(String s){
        int nr = 0;
        String navn = "";
        String form = "";
        String type = "";
        int pris = 0;
        int mengde = 0;
        int styrke = 0;

        if(s.length() > 0){
            String[] parts = s.split(", ");
            nr = Integer.parseInt(parts[0]);
            navn = parts[1];
            form = parts[2];
            type = parts[3];
            pris = Integer.parseInt(parts[4]);
            mengde = Integer.parseInt(parts[5]);
            legemiddelTeller = nr;
	    //Her sjekker type og form for aa opprette objekt av riktig klasse.
	    //Objektet legges til slutt til i tabell.
            if(type.equals("a")){
                styrke = Integer.parseInt(parts[6]);
                if(form.equals("pille")){
                    lm = new LegemidlerAPiller(navn, nr, pris, type, mengde, styrke);
                    lmt.setObject(nr, lm);
                }else if(form.equals("injeksjon")){
                    lm = new LegemidlerAInjeksjon(navn, nr, pris, type, mengde, styrke);
                    lmt.setObject(nr, lm);
                }else if(form.equals("liniment")){
                    lm = new LegemidlerALiniment(navn, nr, pris, type, mengde, styrke);
                    lmt.setObject(nr, lm);
                }else{
                    System.out.println(form + " er ikke gyldig");
                }

            }else if(type.equals("b")){
                styrke = Integer.parseInt(parts[6]);
                if(form.equals("pille")){
                    lm = new LegemidlerBPiller(navn, nr, pris, type, mengde, styrke);
                    lmt.setObject(nr, lm);
                }else if(form.equals("injeksjon")){
                    lm = new LegemidlerBInjeksjon(navn, nr, pris, type, mengde, styrke);
                    lmt.setObject(nr, lm);
                }else if(form.equals("liniment")){
                    lm = new LegemidlerBLiniment(navn, nr, pris, type, mengde, styrke);
                    lmt.setObject(nr, lm);
                }else{
                    System.out.println(form + " er ikke gyldig");
                }
            }else if(type.equals("c")){
                if(form.equals("pille")){
                    lm = new LegemidlerCPiller(navn, nr, pris, type, mengde);
                    lmt.setObject(nr, lm);
                }else if(form.equals("injeksjon")){
                    lm = new LegemidlerCInjeksjon(navn, nr, pris, type, mengde);
                    lmt.setObject(nr, lm);
                }else if(form.equals("liniment")){
                    lm = new LegemidlerCLiniment(navn, nr, pris, type, mengde);
                    lmt.setObject(nr, lm);
                }else{
                    System.out.println(form + " er ikke gyldig");
                }
            } else {
                System.out.println(type + " er ikke gyldig");
            }

        }
    }
    //Oppretter legeobjekter etter innlesing fra fil
    //Tar imot en string, splitter og lagrer data i variabler
    private void parseLeger(String s){
        String navn = "";
        boolean spesialist = false;
        int avtalenr = 0;
        if(s.length() > 0){
            String[] parts = s.split(", ");
            navn = parts[0];
            if(parts[1].equals("1")){
                spesialist = true;
            }
            avtalenr = Integer.parseInt(parts[2]);
	    //Oppretter legeobjekt og setter det inn i liste.
           sel.set(new Leger(navn, spesialist, avtalenr)); 
        }
    }
    //Oppretter reseptobjekter etter innlesing fra fil
    //Tar imot en string, splitter og lagrer dara i variabler
    private void parseResepter(String s){
        int nr = 0;
        String type = "";
        int personnr = 0;
        Leger lege = null;
        Legemidler legemiddel = null;
        int reit = 0;

        if(s.length() > 0){
            String[] parts = s.split(", ");
            nr = Integer.parseInt(parts[0]);
            type = parts[1];
            personnr = Integer.parseInt(parts[2]);
            lege = sel.find(parts[3]);
            legemiddel = lmt.findObject(Integer.parseInt(parts[4]));
            reit = Integer.parseInt(parts[5]);
            reseptTeller = nr;
	    //Oppretter nytt resept og legger til i lister.
            res = new Resepter(nr, type, personnr, lege, legemiddel, reit);
            t.findObject(personnr).getResept().set(res);
            erl.set(res);
        }
    }
}
