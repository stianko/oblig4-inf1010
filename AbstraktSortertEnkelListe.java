// Grensesnitt som setter inn et element og kan returnere et element basert paa en
// String som parameter
public interface AbstraktSortertEnkelListe<T extends Comparable & Lik>{
    public void set(T t);
    public T find(String s);
}
