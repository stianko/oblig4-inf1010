// Grensesnitt som definerer metoder for avtalelege
public interface AvtaleLege{
    public void setAvtaleLege();
    public boolean getAvtaleLege();
    public int getAvtaleNr();
}
