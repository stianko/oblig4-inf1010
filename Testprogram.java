class Testprogram{
    public static void main(String[] args){
        Test test = new Test();
    }
}

class Test{
    TestObjekt to1, to2, to3;
    Tabell<TestObjekt> tt = new Tabell<TestObjekt>(400);
    SortertEnkelListe<Leger> sel = new SortertEnkelListe<>();
    EldsteForstReseptListe y = new EldsteForstReseptListe();
    Resepter r1, r2, r3;
    Leger lege1, lege2, lege3, lege4;
    Test(){
        r1 = new Resepter(1);
        r2 = new Resepter(2);
        r3 = new Resepter(3);

        y.set(r1);
        y.set(r2);
        y.set(r3);

       // Resepter rTemp = y.find(2);
        //System.out.println(rTemp.getNr());

        for(Resepter r : y){
           System.out.println(r.getNr());

        }

        lege1 = new Leger("Bian", true);
        lege2 = new Leger("Sathias", true);
        lege3 = new Leger("Aohn", false);
        lege4 = new Leger("Xartine", true);

        sel.set(lege1);
        sel.set(lege2);
        sel.set(lege3);
        sel.set(lege4);
       
        for(Leger l : sel){
            System.out.println(l.getNavn());
        }

        /*for(int i = 0;i<10;i++){
            if(sel.iterator().hasNext()){
        //while(sel.iterator().hasNext()){
                Leger temp = (Leger) sel.iterator().next();
                System.out.println(temp.getNavn());
            }
        }*/

        to1 = new TestObjekt("nr1");
        to2 = new TestObjekt("nr2");
        to3 = new TestObjekt("nr3");
        tt.setObject(280,to3);
        tt.setObject(0,to1);
        tt.setObject(2,to2);

      //for(int i = 0; i < 10; i++){
        while(tt.iterator().hasNext()){
            System.out.println(tt.iterator().next().getNavn());
        }
    }
}

class TestObjekt{
    private String navn;

    TestObjekt(String s){
        this.navn = s;
    }

    public String getNavn(){
        return navn;
    }
}
