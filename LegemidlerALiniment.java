// Subklasse for narkotiske midler (A) av typen liniment
class LegemidlerALiniment extends LegemidlerA implements Liniment{
    LegemidlerALiniment(String navn, int nummer, int pris, String type, int mengde, int narkotisk){
        super(narkotisk, navn, nummer, pris, type, mengde);
     } 
}
