// Beholder for resepter

import java.util.*;

public class EnkelReseptListe implements Iterable<Resepter>{
    protected Node first, last;
    protected int antall;

    protected class Node{
        Node(Resepter resept) {
            objekt = resept;
        }
        Node neste;
        Resepter objekt;
    }

    // Setter inn objekt i listen
    public void set(Resepter resept){
        Node n = new Node(resept);
        Node previous;
        antall++;

        if(last == null){
            first = n;
            last = n;
            n.neste = null;
            antall++;
        }else{
            previous = last;
            previous.neste = n;
            last = n;
            antall++;
        }
	
    }
    // Henter et objekt med en String som parameter
    public Resepter find(int i){
        for(Resepter resept : this){
            if(resept.getNr() == i){
                return resept;
            } 
        }
        return null;
    }

    // Returnerer en iterator over resepter i listen
    public Iterator<Resepter> iterator(){
        ListeIterator li = new ListeIterator();
        return li;
    }

    // Iterator for resept-liste
    private class ListeIterator implements Iterator<Resepter>{
        Node neste = null;

        // Constructor for iteratoren
        public ListeIterator(){
            neste = first;
        }
        // Returnerer true om neste element eksisterer
        public boolean hasNext(){
            return neste != null;
        }
        // Returnerer neste resept i listen
        public Resepter next(){
            if(hasNext()){
                Resepter ut = neste.objekt;
                neste = neste.neste;
                return ut;
            } else {
                throw new NoSuchElementException();
            }
        }

        // Kaster et unntak om remove blir kalt
        public void remove(){
            throw new UnsupportedOperationException();
        }
    }

}
