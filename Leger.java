// Klassen implementerer grensnitt for avtalelege, og sammenligne med andre leger, og lik
public class Leger implements AvtaleLege, Lik, Comparable<Leger>{
    private String navn;
    private boolean spesialist;
    private int avtalenr = 0;
    private boolean avtaleLege = false;
    private EldsteForstReseptListe resepter = null;

    public boolean samme(String s){
        if (s.equals(navn)){
            return true;
        }
        return false;
    }

    public String getNavn(){
        return navn;
    }

    public void setAvtaleLege(){
        if(avtalenr != 0){
            avtaleLege = true;
        }
    }

    // Returnerer reseptlisten til denne legen
    public EldsteForstReseptListe getResept(){
        return resepter;
    }

    public boolean getAvtaleLege(){
        return avtaleLege;
    }
    public int getAvtaleNr(){
        return avtalenr;
    }

    // Sammenligner navn paa leger vha. compareTo-metoden i String
    public int compareTo(Leger lege){
        return navn.compareTo(lege.getNavn());
    }

    public boolean getSpesialist(){
        return spesialist;
    }

    Leger(String navn, boolean spesialist, int avtalenr){
        this.navn = navn;
        this.spesialist = spesialist;
        this.avtalenr = avtalenr;
        resepter = new EldsteForstReseptListe();
        setAvtaleLege();
    }
}
