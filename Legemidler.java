public class Legemidler{
    private String navn;
    private int nummer;
    private int pris;
    private String type;
    private int mengde;

    public String getNavn(){
        return navn;
    }

    public int getNr(){
        return nummer;
    }

    public int getPris(){
        return pris;
    }
    public String getType(){
        return type;
    }
    public int getMengde(){
        return mengde;
    }

    // Constructor for klassen legemiddel
    Legemidler(String navn, int nummer, int pris, String type, int mengde){
        this.navn = navn;
        this.nummer = nummer;
        this.pris = pris;
        this.type = type;
        this.mengde = mengde;
       } 
   }
