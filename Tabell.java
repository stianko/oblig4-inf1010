import java.util.*;

//Implementerer nodvendige grensesnitt
public class Tabell<T> implements AbstraktTabell<T>, Iterable<T>{
    private int lengde;
    private T[] tabell;
    private int counter = 0;

    //Setter inn objekt i tabellen
    public boolean setObject(int i, T t){
        if(tabell[i] == null){
            tabell[i] = t;
            return true;
        }
        return false;
    }
    //Finner objekt i tabell basert paa indeks
    public T findObject(int i){
        return tabell[i];
    }
    //Returnerer iterator over listen
    public Iterator<T> iterator(){
	      ListeIterator li = new ListeIterator();
        return li;
    }
    //Konstruktor
    Tabell(int lengde){
	      tabell = (T[])new Object[lengde];
    }
    //Iterator-klassen med nodvendige metoder for iteratoren
    private class ListeIterator implements Iterator<T>{
        public boolean hasNext(){
            while(counter < tabell.length){
                if(tabell[counter] != null){
                    return true;
                }else if(counter == tabell.length-1){
                    counter = 0;
                    return false;
                }else{
                    counter++;
                }
            }
            return false;
        }

        public T next(){
            if(hasNext()){
                counter++;
                return tabell[counter - 1];
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove(){
            throw new UnsupportedOperationException();
        }
    }

}
