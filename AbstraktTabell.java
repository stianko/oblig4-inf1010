// Grensesnitt som beskriver en beholder som tar inn et objekt T med en int som parameter,
// og returnerer et objekt basert paa en int som parameter
public interface AbstraktTabell<T>{
    public boolean setObject(int i, T t);
    public T findObject(int i);
}
