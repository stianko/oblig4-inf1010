// Subklasse for narkotiske midler (A) av typen piller
class LegemidlerAPiller extends LegemidlerA implements Piller{
    LegemidlerAPiller(String navn, int nummer, int pris, String type, int mengde, int narkotisk){
        super(narkotisk, navn, nummer, pris, type, mengde);
     } 
}
