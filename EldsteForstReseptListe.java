// Subklasse av Enkelreseptliste, brukes for aa
// sette inn eldste element foerst i en liste

public class EldsteForstReseptListe extends EnkelReseptListe{

    public void set(Resepter resept){
        Node n = new Node(resept);
        antall++;

        if(last == null){
            first = n;
            last = n;
            n.neste = null;
            antall++;
        }else{
            last.neste = n;
            last = n;
            antall++;
        }
    }
}
