// Subklasse for legemidler uten spesiell klassifikasjon (C) av typen Injeksjon
class LegemidlerCInjeksjon extends LegemidlerC implements Injeksjon{
    LegemidlerCInjeksjon(String navn, int nummer, int pris, String type, int mengde){
        super(navn, nummer, pris, type, mengde);
     } 
}
