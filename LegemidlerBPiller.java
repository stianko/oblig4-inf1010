// Subklasse for vanedannende midler (B) av typen piller
class LegemidlerBPiller extends LegemidlerB implements Piller{
    LegemidlerBPiller(String navn, int nummer, int pris, String type, int mengde, int vanedannende){
        super(vanedannende, navn, nummer, pris, type, mengde);
     } 
}
