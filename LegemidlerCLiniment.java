// Subklasse for legemidler uten spesiell klassifikasjon (C) av typen liniment
class LegemidlerCLiniment extends LegemidlerC implements Liniment{
    LegemidlerCLiniment(String navn, int nummer, int pris, String type, int mengde){
        super(navn, nummer, pris, type, mengde);
     } 
}
