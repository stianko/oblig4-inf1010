// Subklasse for narkotiske midler (A)
public class LegemidlerA extends Legemidler{
      private int narkotisk;
      
      LegemidlerA(int narkotisk, String navn, int nummer, int pris, String type, int mengde){
          super(navn, nummer, pris, type, mengde);
          this.narkotisk = narkotisk;
      }
}
