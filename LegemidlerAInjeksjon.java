// Subklasse for narkotiske midler (A) av typen injeksjon
class LegemidlerAInjeksjon extends LegemidlerA implements Injeksjon{
    LegemidlerAInjeksjon(String navn, int nummer, int pris, String type, int mengde, int narkotisk){
        super(narkotisk, navn, nummer, pris, type, mengde);
     } 
} 
