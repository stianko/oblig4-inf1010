public class Resepter{
    private int nr;
    private Legemidler legemiddel;
    private Leger lege;
    private int personnr;
    private int reit;
    private String type;
    private String reseptFarge;
    //Konstruktor for ny resept
    Resepter(int nr, String type, int personnr, Leger lege, Legemidler legemiddel, int reit){
        this.nr = nr;
        this.type = type;
        this.personnr = personnr;
        this.lege = lege;
        this.legemiddel = legemiddel;
        this.reit = reit;
        lege.getResept().set(this);
    }
    //Returnerer legemiddelet som er tilknyttet resepten
    public Legemidler hentResept(){
        if(reit == 0){
            return null;
        }else{
            reit--;
            return legemiddel;
        }

    }
    public int getNr(){
        return nr;
    }
    public Legemidler getLegemiddel(){
        return legemiddel;
    }
    public int getReit(){
        return reit;
    }
    public Leger getLege(){
        return lege;
    }
    public int getPersonnr(){
        return personnr;
    }
    public String getType(){
        return type;
    }
}
