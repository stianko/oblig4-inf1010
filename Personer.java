public class Personer{
    private String navn;
    private int personnr;
    private YngsteForstReseptListe resepter = null;
    private String kjonn;

    public String getNavn(){
        return navn;
    }

    public int getNummer(){
        return personnr;
    }
    // Returnerer reseptlisten til denne personen
    public YngsteForstReseptListe getResept(){
        return resepter;
    }

    public String getKjonn(){
        return kjonn;
    }

    Personer(String navn, int personnr, String kjonn){
        this.navn = navn;
        this.personnr = personnr;
        this.kjonn = kjonn;
        resepter = new YngsteForstReseptListe();
    }
}
