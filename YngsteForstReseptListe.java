public class YngsteForstReseptListe extends EnkelReseptListe{
    //Objekter av denne klassen finnes i personer.
    //Brukes til aa holde personens resepter

    //Setter inn resept i liste
    public void set(Resepter resept){
        Node n = new Node(resept);
        antall++;

        if(last == null){
            first = n;
            last = n;
            n.neste = null;
            antall++;
        }else{
            n.neste = first;
            first = n;
            antall++;
        }
    }
}
