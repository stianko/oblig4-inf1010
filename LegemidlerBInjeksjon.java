// Subklasse for vanedannende midler (B) av typen injeksjon
class LegemidlerBInjeksjon extends LegemidlerB implements Injeksjon{
    LegemidlerBInjeksjon(String navn, int nummer, int pris, String type, int mengde, int vanedannende){
        super(vanedannende, navn, nummer, pris, type, mengde);
     } 
}
